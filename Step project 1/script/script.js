//------------Our Services Section-----------------//

const collectionOfMenuItems = document.querySelectorAll('.our-services__navigation__menu__item__link');
const arrayOfCollectionOfMenuItems = [...collectionOfMenuItems];
const collectionOfSectionsDescription = document.querySelectorAll('.our-services__section');
const ourServicesNavigationMenu = document.querySelector('.our-services__navigation__menu');

let activeMenuElement = null;

collectionOfMenuItems.forEach(element => {
    if (element.classList.contains('item__link-active')) {
        activeMenuElement = element;
        return null;
    }
})

let activeMenuElementDescription = collectionOfSectionsDescription[arrayOfCollectionOfMenuItems.indexOf(activeMenuElement)];

const ourServicesNavigationMenuToggle = event => {
    event.preventDefault();
    activeMenuElement.classList.remove('item__link-active');
    activeMenuElementDescription.classList.remove('description-active');
    event.target.classList.add('item__link-active');
    activeMenuElement = event.target;
    activeMenuElementDescription = collectionOfSectionsDescription[arrayOfCollectionOfMenuItems.indexOf(activeMenuElement)];
    activeMenuElementDescription.classList.add('description-active')
}

ourServicesNavigationMenu.addEventListener('click', ourServicesNavigationMenuToggle);

//--------------Our Amazing Work Gallery----------------//

const graphicDesignCollection = document.querySelectorAll('.graphic-design');
const webDesignCollection = document.querySelectorAll('.web-design');
const landingPageCollection = document.querySelectorAll('.landing-page');
const wordpressCollection = document.querySelectorAll('.wordpress');
const filtersNavigationCollection = document.querySelectorAll('.filters-navigation__menu__item__link')
const filtersNavigationMenu = document.querySelector('.filters-navigation__menu');
const addMoreButton = document.querySelector('.btn__load-more');

let activeFilterKey = 'all';
let activeFilter = filtersNavigationCollection[0];
activeFilter.classList.add('menu-item__link-active');
let addMoreCounter = 0;

const filterToggle = event => {
    event.preventDefault();
    let classList = event.target.classList.toString();
    activeFilter.classList.remove('menu-item__link-active');
    activeFilter = event.target;
    activeFilter.classList.add('menu-item__link-active');
    switch (classList) {
        case 'filters-navigation__menu__item__link a':
            allGallery();
            break;
        case 'filters-navigation__menu__item__link gd':
            graphicDesignGallery();
            break;
        case 'filters-navigation__menu__item__link wd':
            webDesignGallery();
            break;
        case 'filters-navigation__menu__item__link lp':
            landingPageGallery();
            break;
        case 'filters-navigation__menu__item__link w':
            wordpressGallery();
            break;
        default:
            break;
    }
}

filtersNavigationMenu.addEventListener('click', filterToggle);

const imagesDeactivate = (key) => {
    switch (key) {
        case 'all':
            for (let index = 0; index < 13; index++) {
                graphicDesignCollection[index].classList.remove('active');
                webDesignCollection[index].classList.remove('active');
                landingPageCollection[index].classList.remove('active');
                wordpressCollection[index].classList.remove('active');
            }
            break;
        case 'graphicDesign':
            for (let index = 0; index < 37; index++) {
                graphicDesignCollection[index].classList.remove('active');
            }
            break;
        case 'webDesign':
            for (let index = 0; index < 37; index++) {
                webDesignCollection[index].classList.remove('active');
            }
            break;
        case 'landingPage':
            for (let index = 0; index < 37; index++) {
                landingPageCollection[index].classList.remove('active');
            }
            break;
        case 'wordpress':
            for (let index = 0; index < 36; index++) {
                wordpressCollection[index].classList.remove('active');
            }
            break;
    }
}

const allGallery = () => {
    if (activeFilterKey !== 'all') {
        imagesDeactivate(activeFilterKey);
        activeFilterKey = 'all';
        if (addMoreCounter === 2) {
            addMoreButton.style.display = 'block';
            addMoreCounter = 0;
        }
    }
    let activePicturesCounter = 0;
    for (let element of graphicDesignCollection) {
        if (element === graphicDesignCollection[0]) continue;
        if (!element.classList.contains('active') && activePicturesCounter <3) {
            activePicturesCounter++;
            element.classList.add('active');
        }
        if (activePicturesCounter === 3) {
            activePicturesCounter = 0;
            break;
        }
    }
    for (let element of webDesignCollection) {
        if (element === webDesignCollection[0]) continue;
        if (!element.classList.contains('active') && activePicturesCounter <3) {
            activePicturesCounter++;
            element.classList.add('active');
        }
        if (activePicturesCounter === 3) {
            activePicturesCounter = 0;
            break;
        }
    }
   for (let element of landingPageCollection) {
       if (element === landingPageCollection[0]) continue;
       if (!element.classList.contains('active') && activePicturesCounter <3) {
            activePicturesCounter++;
            element.classList.add('active');
        }
       if (activePicturesCounter === 3) {
            activePicturesCounter = 0;
            break;
       }
    }
    for (let element of wordpressCollection) {
        if (element === wordpressCollection[0]) continue;
        if (!element.classList.contains('active') && activePicturesCounter <3) {
            activePicturesCounter++;
            element.classList.add('active');
        }
        if (activePicturesCounter === 3) {
            activePicturesCounter = 0;
            break;
        }
    }
}

const graphicDesignGallery = () => {
    if (activeFilterKey !== 'graphicDesign') {
        imagesDeactivate(activeFilterKey);
        activeFilterKey = 'graphicDesign';
        if (addMoreCounter === 2) {
            addMoreButton.style.display = 'block';
            addMoreCounter = 0;
        }
    }
    let activePicturesCounter = 0;
    for (let element of graphicDesignCollection) {
        if (!element.classList.contains('active') && activePicturesCounter <12) {
            activePicturesCounter++;
            element.classList.add('active');
        }
        if (activePicturesCounter === 12) {
            activePicturesCounter = 0;
            break;
        }
    }
}

const webDesignGallery = () => {
    if (activeFilterKey !== 'webDesign') {
        imagesDeactivate(activeFilterKey);
        activeFilterKey = 'webDesign';
        if (addMoreCounter === 2) {
            addMoreButton.style.display = 'block';
            addMoreCounter = 0;
        }
    }
    let activePicturesCounter = 0;
    for (let element of webDesignCollection) {
        if (!element.classList.contains('active') && activePicturesCounter <12) {
            activePicturesCounter++;
            element.classList.add('active');
        }
        if (activePicturesCounter === 12) {
            activePicturesCounter = 0;
            break;
        }
    }
}

const landingPageGallery = () => {
    if (activeFilterKey !== 'landingPage') {
        imagesDeactivate(activeFilterKey);
        activeFilterKey = 'landingPage';
        if (addMoreCounter === 2) {
            addMoreButton.style.display = 'block';
            addMoreCounter = 0;
        }
    }
    let activePicturesCounter = 0;
    for (let element of landingPageCollection) {
        if (!element.classList.contains('active') && activePicturesCounter <12) {
            activePicturesCounter++;
            element.classList.add('active');
        }
        if (activePicturesCounter === 12) {
            activePicturesCounter = 0;
            break;
        }
    }
}

const wordpressGallery = () => {
    if (activeFilterKey !== 'wordpress') {
        imagesDeactivate(activeFilterKey);
        activeFilterKey = 'wordpress';
        if (addMoreCounter === 2) {
            addMoreButton.style.display = 'block';
            addMoreCounter = 0;
        }
    }
    let activePicturesCounter = 0;
    for (let element of wordpressCollection) {
        if (!element.classList.contains('active') && activePicturesCounter <12) {
            activePicturesCounter++;
            element.classList.add('active');
        }
        if (activePicturesCounter === 12) {
            activePicturesCounter = 0;
            break;
        }
    }
}

allGallery();

const addMorePictures = (btn) => {
    switch (activeFilterKey) {
        case 'all':
            allGallery();
            break;
        case 'graphicDesign':
            graphicDesignGallery();
            break;
        case 'webDesign':
            webDesignGallery();
            break;
        case 'landingPage':
            landingPageGallery();
            break;
        case 'wordpress':
            wordpressGallery();
            break;
    }
    addMoreCounter++;
    if (addMoreCounter === 2) btn.style.display = 'none';
};