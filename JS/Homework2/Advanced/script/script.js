//-----------Input
let lowerRangeLimit = prompt("Please enter lower range limit (whole positive number): ");
let upperRangeLimit = prompt("Please enter upper range limit (whole positive number): ");

let lowerRangeLimitNumber = +lowerRangeLimit;
let upperRangeLimitNumber = +upperRangeLimit;

//-----------Input validation
while (
    isNaN(lowerRangeLimitNumber) || isNaN(upperRangeLimitNumber) ||
        lowerRangeLimitNumber < 0 || upperRangeLimitNumber < 0 ||
        Math.round(lowerRangeLimitNumber) !== lowerRangeLimitNumber ||
        Math.round(upperRangeLimitNumber) !== upperRangeLimitNumber ||
        upperRangeLimitNumber < lowerRangeLimitNumber || upperRangeLimitNumber === lowerRangeLimitNumber
    )
{
    alert("Incorrect input! Both numbers should be whole and positive; upper limit should be greater than lower limit. Please re-enter range limits.");
    lowerRangeLimitNumber = +prompt("Please enter lower range limit (whole positive number): ");
    upperRangeLimitNumber = +prompt ("Please enter upper range limit (whole positive number):");
}

//-----------Main algorithm
let flagIsPrimeNumber = true;
let flagPrimeNumbersFound = false;
for (let i = lowerRangeLimitNumber; i < upperRangeLimitNumber + 1; i++)
{
    for (let multiple = 2; multiple < Math.floor(i/2)+1; multiple ++)
    {
        if (i%multiple === 0)
        {
            flagIsPrimeNumber = false;
            break;
        }
    }
    if (flagIsPrimeNumber && i > 1) {
        console.log(i);
        flagPrimeNumbersFound = true;
    }
    flagIsPrimeNumber = true;
}
if (!flagPrimeNumbersFound) console.log("No prime numbers were found within the range from "+lowerRangeLimitNumber+" to "+upperRangeLimitNumber);

