//-----------Input
let rangeLimit = prompt("Please enter any whole number greater than 0: ");
let rangeLimitNumber = +rangeLimit;

//-----------Input validation for NaN and fractional number
while (isNaN(rangeLimitNumber) || (Math.round(rangeLimitNumber) !== rangeLimitNumber) || rangeLimitNumber === 0 || rangeLimitNumber < 0) {
    rangeLimitNumber = +prompt(rangeLimit + " is not correct input type. Please enter any whole number greater than 0: ");
}

//-----------Main algorithm
let flagNumbersFound = false;
for (let i = 1; i < rangeLimitNumber+1; i++) {
    if (i%5 === 0) {
        console.log (i);
        flagNumbersFound = true;
    }
}

//-----------Output
if (flagNumbersFound === false) console.log("Sorry no numbers multiple of 5 are found in the range from 0 to "+rangeLimitNumber);