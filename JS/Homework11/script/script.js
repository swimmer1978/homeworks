const btnCollection = document.querySelector('.btn-wrapper').querySelectorAll('.btn');

let currentKey = null;

const handler = event => {
    btnCollection.forEach(element => {
        if (!currentKey && event.key.toLowerCase() === element.dataset.setKeyname) {
            currentKey = element;
            element.style.backgroundColor = 'royalblue'
            return null;
        }
        if (event.key.toLowerCase() === element.dataset.setKeyname && event.key.toLowerCase() !== currentKey.dataset.setKeyname) {
                currentKey.style.backgroundColor = 'black';
                currentKey = element;
                element.style.backgroundColor = 'royalblue'
                return null;
            }
    });
};

document.addEventListener('keyup', handler);

