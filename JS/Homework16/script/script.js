//---------Functions declarations

//--input validation
function validateNumericalInput (number) {
return Number.isNaN(number) || Number.isNaN(parseInt(number)) || Math.round(Number(number)) !== Number(number)
}

function validateFiboOrderNumber (fiboOrderNumber) {
return Number.isNaN(fiboOrderNumber) || Number.isNaN(parseInt(fiboOrderNumber)) || Math.round(Number(fiboOrderNumber)) !== Number(fiboOrderNumber) || Number(fiboOrderNumber) < 0 || Number(fiboOrderNumber) === 0
}

//--input reentry
function reenterNumericalInput (number, numberId) {
    let fiboOrderNumber = "1";
    if (numberId === 'N#') fiboOrderNumber = number;
    while (validateNumericalInput(number) || validateFiboOrderNumber(fiboOrderNumber)) {
        number = prompt(`Некорректный ввод ${numberId}. Введите ${numberId} еще раз! ${numberId} = `);
        if (numberId === 'N#') fiboOrderNumber = number;
    }
    return number;
}

//--MAIN FUNCTION
function calcNthFibonacciNumber (number1, number2, fiboOrderNumber) {
    if (fiboOrderNumber === 1) return number1;
    if (fiboOrderNumber === 2) return number2;
    return calcNthFibonacciNumber(number1, number2,fiboOrderNumber-1) + calcNthFibonacciNumber(number1,  number2, fiboOrderNumber-2);
}

//---------Main algorithm
let number1 = prompt("N1 = ");
number1 = reenterNumericalInput(number1, 'N1');
let number2 = prompt("N2 = ");
number2 = reenterNumericalInput(number2, 'N2');
let fiboOrderNumber = prompt("Введите порядковый номер числа для расчета N# = ");
fiboOrderNumber = reenterNumericalInput(fiboOrderNumber, 'N#');

let numberNumber1 = Number(number1);
let numberNumber2 = Number(number2);
let numberFiboOrderNumber = Number(fiboOrderNumber);

console.log(calcNthFibonacciNumber(numberNumber1, numberNumber2, numberFiboOrderNumber));