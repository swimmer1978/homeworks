function filterBy(array, dataType) {
    if (dataType === 'null') return array.filter(element => element);
    if (dataType === 'object') return array.filter(element => typeof element !== dataType || element === null);
    return array.filter(element =>
        typeof element !== dataType
    );
}

let array = ['hello', 'world', 23, '23', null, false, true];
console.log(filterBy(array, 'boolean'));
