//----------Data input and verification

let userName = prompt("Please enter your name:");
if (!userName) {
    while (!userName) {
        userName = prompt("You didn't enter your name. Please enter your name:")
    }
}

let userAge = +prompt("Please enter your age (years):");
if (isNaN(userAge)) {
    while (isNaN(userAge)) {
        userAge = +prompt("You entered invalid age. Please enter your age as a number (years):")
    }
}

//----------Main algorithm
if (userAge < 18) {
    alert(userName + ", you are not allowed to visit website!")
}
else if (userAge >= 18 && userAge <= 22 && confirm(userName + ", are sure you want to continue?")) {
    alert("Welcome " + userName + "!")
}
else if (userAge > 22) {
    alert("Welcome " + userName + "!")
}
else {
    alert(userName + ", you are not allowed to visit website!");
}