//----------Functions declarations

function validateNumericalInput (number) {
    return (Number.isNaN(number) || Number.isNaN(parseInt(number)) || Math.round(Number(number)) !== Number(number) || Number(number) <= 0);
}

function calculateFactorial (n) {
//    console.log(n);
    if (n === 1) return 1;
    return n*calculateFactorial(n-1);
}

//----------Input
let n = prompt("Enter n: ");
while (validateNumericalInput(n)) {
    n = prompt("Некорректный ввод! Введите целое положительное число не равное 0: ");
}
let numberN = Number(n);

//----------Main algorithm
console.log(calculateFactorial(numberN));





