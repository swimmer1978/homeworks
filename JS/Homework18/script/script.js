function cloneObject(object){
    let clonedObject = Object.create(null);
        for (let key in object) {
            if (Array.isArray(object[key])) {
                clonedObject[key] = [];
                for (let arrayKey in object[key]) {
                    clonedObject[key][arrayKey] = object[key][arrayKey];
                }
            }
            if (!Array.isArray(object[key])&&!(typeof object[key] === 'object')) {
                clonedObject[key] = object[key];
            } else {clonedObject[key] = cloneObject(object[key])}
        }
        return clonedObject;
}

let clonedObject = cloneObject({name:'max', lastname:'leshchenko', homeworks:{math:5, history:10,}, marks:[4,5,6,7,8]});
console.log(clonedObject);