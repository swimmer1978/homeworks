//---------Function declaration

function createNewUser() {
    let firstName = prompt("Введите Имя: ");
    let lastName = prompt ("Введите Фамилию: ");
    let newUser = {
        firstName: firstName,
        lastName: lastName,
        getLogin: function () {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase()
        },

        setFirstName: function (firstName) {
            Object.defineProperty(this, 'firstName', {
                writable: true
            })
            this.firstName = firstName;
            Object.defineProperty(this, 'firstName', {
                writable: false
            })
        },

        setLastName: function (lastName) {
            Object.defineProperty(this, 'lastName', {
                writable: true
            })
            this.lastName = lastName;
            Object.defineProperty(this, 'lastName', {
                writable: false
            })
        },

    }
    Object.defineProperty (newUser, 'firstName', {
        writable: false
    });
    Object.defineProperty (newUser, 'lastName', {
        writable: false
    });
    return newUser;
}

//----------Main algorithm

let newUser = createNewUser();
let login = newUser.getLogin();

console.log(newUser);
console.log(login);
