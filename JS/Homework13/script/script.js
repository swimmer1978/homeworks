const colorChange = document.querySelectorAll('span, a, h3, button');
const backgroundChange = document.querySelectorAll('.main__most-popular-posts__item__picture-link, .hot-news-div');
const buttonSubscribeNow = document.querySelector('.header__the-latest-news-subscribe-now');
const buttonBestArticles = document.querySelector('.header__the-latest-news-best-articles')
const mainBackgroundChange = document.querySelector('main');

const changeElements = () => {
    colorChange.forEach(element => {
        element.classList.toggle('dark');
    })

    backgroundChange.forEach((element => {
        element.classList.toggle('dark-background')
    }))

    buttonSubscribeNow.classList.toggle('dark-button-subscribe-now');
    buttonBestArticles.classList.toggle('dark-button-best-articles');

    mainBackgroundChange.classList.toggle('main-dark');
}

const changeTheme = element => {

    changeElements();

    if (element.innerText === 'DARK') {
        element.innerText = 'LIGHT';
        element.style.backgroundColor = 'aqua';
        element.style.color = 'black';
        localStorage.setItem('theme', 'dark');
    }
    else {
        element.innerText = 'DARK';
        element.style.backgroundColor = '#23201D';
        element.style.color = 'coral';
        localStorage.setItem('theme', 'light');
    }
}

if (localStorage.getItem('theme') === 'dark') changeTheme(document.querySelector('.header__change-theme-button'));
