//--------------Function declaration
function createNewStudent () {
    let firstName = prompt('Введите имя студента: ');
    let lastName = prompt('Введите фамилию студента: ');
    let newStudent = Object.create(null);
    newStudent.firstName = firstName;
    newStudent.lastName = lastName;
    Object.defineProperty(newStudent, 'firstName', {
        writable: false
    })
    Object.defineProperty(newStudent, 'lastName', {
        writable: false
    })
    newStudent.setFirstName = function(newFirstName) {
        Object.defineProperty(newStudent, 'firstName', {
            writable: true
        })
        this.firstName = newFirstName;
        Object.defineProperty(newStudent, 'firstName', {
            writable: false
        })
    }
    newStudent.setLastName = function(newLastName) {
        Object.defineProperty(newStudent, 'lastName', {
            writable: true
        })
        this.lastName = newLastName;
        Object.defineProperty(newStudent, 'lastName', {
            writable: false
        })
    }
    newStudent.tabel = Object.create(null);
    return newStudent;
}

let newStudent = createNewStudent();
let subjectName = prompt('Введите название предмета: ');
 while (subjectName) {
     newStudent.tabel[subjectName] = +prompt(`Введите оценку по предмету "${subjectName}":`);
     subjectName = prompt('Введите название следующего предмета: ');
 }

 let countFailedSubjects = null;
 let sumOfScores = null;
 for (let key in newStudent.tabel) {
     if (newStudent.tabel[key] < 4) countFailedSubjects++;
     sumOfScores += newStudent.tabel[key];
 }

 if (!countFailedSubjects) alert(`Студент ${newStudent.firstName} ${newStudent.lastName} переведен на следующий курс! Поздравляем!`);

 if (sumOfScores / Object.keys(newStudent.tabel).length > 7) alert(`Студенту ${newStudent.firstName} ${newStudent.lastName} назначена степендия. Поздравляем!`);
