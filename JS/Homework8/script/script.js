/* ---------Getting all elements necessary--------- */

const currentPrice =document.querySelector('.currentPrice');
const removeBtn = document.querySelector('.removeBtn');
const priceInputField = document.querySelector('.inputField');
const warningTag = document.querySelector('.warningTag');

/* ---------Main Algorithm--------- */

priceInputField.addEventListener('focus', () => {

  priceInputField.style.border = '1px solid green';
  priceInputField.style.color = 'black';

});

priceInputField.addEventListener('blur', () => {

  if (priceInputField.value <= 0 && priceInputField.value !== '') {
    priceInputField.style.border = '1px solid red';
    warningTag.style.display = "block";
    priceInputField.value = '';
    currentPrice.style.display = 'none';
    removeBtn.style.display = 'none';
    return null;
  }
  if (priceInputField.value
      !== '') {
    warningTag.style.display = 'none';
    currentPrice.innerText = `Current price: $ ${priceInputField.value}`;
    currentPrice.style.display = 'inline-block';
    removeBtn.style.display = 'inline-block';
    priceInputField.style.color = 'green';
  }
  priceInputField.style.border = '1px solid black';

});

removeBtn.addEventListener('click', () => {

  currentPrice.style.display = 'none';
  removeBtn.style.display = 'none';
  priceInputField.value = '';
  priceInputField.style.border = '1px solid black';

});