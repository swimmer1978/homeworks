const listenerDiv = document.querySelector('.tabs');
const tabsContent = document.querySelectorAll('.tab-text');

const handler = event => {
        const eventTarget = event.target;
        if (!eventTarget.classList.contains('tabs-title active')) {

            document.querySelector('.tabs-title.active').classList.remove('active');
            document.querySelector('.tab-text.active').classList.remove('active');

            eventTarget.classList.add('active');
            tabsContent.forEach(element => {
                if (element.dataset.setTabname === eventTarget.innerText) {
                    element.classList.add('active');
                }
            })
        }
};

listenerDiv.addEventListener('click', handler);
