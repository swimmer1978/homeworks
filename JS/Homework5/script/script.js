//---------Function declaration

function createNewUser() {
    let firstName = prompt("Введите Имя: ");
    let lastName = prompt ("Введите Фамилию: ");
    let birthday = prompt ("Введите дату вашего рождения в формате - dd.mm.yyyy: ");
    let newUser = {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday,

        getLogin: function () {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase()
        },

        getPassword: function () {
            return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase()+this.birthday.substring(6, 10);
        },

        getAge: function () {
            let birthday = this.birthday.substring(6, 10) + "-" + this.birthday.substring(3, 5) + "-" + this.birthday.substring(0, 2);
            let birthdayDate = new Date(birthday);
            let currentDate = new Date();
            let userAge = currentDate.getFullYear() - birthdayDate.getFullYear();
            if (currentDate.getMonth() === birthdayDate.getMonth() && currentDate.getDate() < birthdayDate.getDate()) return userAge-1;
            if (currentDate.getMonth() < birthdayDate.getMonth()) return userAge-1;
            return userAge;
       },

      setFirstName: function (firstName) {
            Object.defineProperty(this, 'firstName', {
                writable: true
            })
            this.firstName = firstName;
            Object.defineProperty(this, 'firstName', {
                writable: false
            })
        },

        setLastName: function (lastName) {
            Object.defineProperty(this, 'lastName', {
                writable: true
            })
            this.lastName = lastName;
            Object.defineProperty(this, 'lastName', {
                writable: false
            })
        },

    }
    Object.defineProperty (newUser, 'firstName', {
        writable: false
    });
    Object.defineProperty (newUser, 'lastName', {
        writable: false
    });
    return newUser;
}

//----------Main algorithm

let newUser = createNewUser();
let login = newUser.getLogin();
let password = newUser.getPassword();
let age = newUser.getAge();

console.log(newUser);
console.log(login);
console.log(password);
console.log(age);
