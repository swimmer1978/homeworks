const displayAsAList = function (array = [], parentNodeElement = document.querySelector('body') ) {
    const list = document.createElement('ul');
    const formattedArray = array.map(element => {
        const listItem = document.createElement('li');
        if (Array.isArray(element)) {
            listItem.innerText = 'Nested Array';
            displayAsAList(element, listItem);
            return listItem;
        }
        listItem.innerText = element;
        return listItem;
    })
    formattedArray.forEach(element => {
        list.append(element);
    })
    parentNodeElement.append(list);
}

let array = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

displayAsAList(array);


let timer = document.createElement('div');
document.body.append(timer);
for (let i = 3; i>=0; i-- ) {
    setTimeout(() => {
        timer.innerText = `Page will clear out in: ${i} seconds`
        if (i === 0) {
            let list = document.querySelector('ul');
            list.remove();
            timer.remove();
        }
    }, 1000 * (4 - i));
}



