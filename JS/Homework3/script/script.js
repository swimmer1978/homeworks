//----------Functions declarations
function validateNumericalInput (number1, number2) {
    let number1Number = Number(number1);
    let number2Number = Number(number2);
    return !(isNaN(number1Number) || isNaN(parseInt(number1)) || isNaN(number2Number) || isNaN(parseInt(number2)));
}

function validateArithmeticAction (arithmeticAction) {
    return (arithmeticAction === "*" || arithmeticAction === "/" || arithmeticAction === "+" || arithmeticAction === "-");
}

function arithmeticActionResult (number1, number2, arithmeticAction) {
    if (arithmeticAction === "*") return `${number1} ${arithmeticAction} ${number2} = ${Number(number1)*Number(number2)}`;
    if (arithmeticAction === "/") return `${number1} ${arithmeticAction} ${number2} = ${Number(number1)/Number(number2)}`;
    if (arithmeticAction === "+") return `${number1} ${arithmeticAction} ${number2} = ${Number(number1)+Number(number2)}`;
    if (arithmeticAction === "-") return `${number1} ${arithmeticAction} ${number2} = ${Number(number1)-Number(number2)}`;
}

//----------Input
let number1 = prompt("Введите 1-е число: ");
let number2 = prompt("Введите 2-е число: ");

while (!validateNumericalInput(number1,number2)) {
    number1 = prompt("Некорректный ввод данных. Введите 1-e число еще раз: ");
    number2 = prompt("Некорректный ввод данных. Введите 2-e число еще раз: ");
}

let arithmeticAction = prompt(`Выберите одно из 4-х арифметических действий: *, /, +, - для введенных чисел: ${number1} и ${number2}: `);

while (!validateArithmeticAction(arithmeticAction)) {
    arithmeticAction = prompt(`Некорректный ввод данных. Введите символ одного из 4-х арифметических действий: *, /, +, - для чисел: ${number1} и ${number2}: `);
}

console.log(arithmeticActionResult(number1, number2, arithmeticAction));
