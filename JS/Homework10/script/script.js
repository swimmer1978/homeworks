const toggleFunction = event => {
    event.style.display = 'none';
    let activeIcon = null;
    let activeType = '';
    if (event.classList.contains('fa-eye')) {
        activeIcon = event.parentNode.querySelector('.fa-eye-slash');
        activeType = 'text';
        } else {
        activeIcon = event.parentNode.querySelector('.fa-eye');
        activeType = 'password';
        }

    activeIcon.style.display = 'block';
    event.parentNode.querySelector('input').type = activeType;
}

const submitBtn = document.querySelector('.btn');
const submitFunction = event => {
    event.preventDefault();
    const passwords = document.querySelectorAll('input');

    const passwordsArray = [];
    passwords.forEach(element => passwordsArray.push(element.value));

    const passwordsSet= new Set(passwordsArray);

    const passwordsArrayUnique = [];
    passwordsSet.forEach(element => passwordsArrayUnique.push(element));

    if (passwordsArrayUnique.includes('') || passwordsArrayUnique.length > 1) {
        document.querySelector('.warning-message').style.display = 'block';
        return null;
    }

    if (document.querySelector('.warning-message').style.display === 'block')
        document.querySelector('.warning-message').style.display = 'none';

    setTimeout(() => alert('You are welcome'), 100);
}

submitBtn.addEventListener('click', submitFunction);

