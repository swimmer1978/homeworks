const imagesToShowCollection = document.querySelector('.images-wrapper').querySelectorAll('.image-to-show');
const btnContinue = document.querySelector('.continue');
const btnStop = document.querySelector('.stop');
btnContinue.disabled = true;

let imagesIterator = 0;
let currentImage = imagesToShowCollection[imagesIterator];
currentImage.style.display = 'block';


const slider = () => {
    imagesIterator++;
    if (imagesIterator === imagesToShowCollection.length) imagesIterator = 0;
    currentImage.style.display = 'none';
    imagesToShowCollection[imagesIterator].style.display = 'block';
    currentImage = imagesToShowCollection[imagesIterator];
    timer();
}

let displaySlider = setInterval(slider, 3010);

const stop = (button) => {
    clearInterval(displaySlider);
    button.disabled = true;
    btnContinue.disabled = false;
    ;
}

const startContinue = (button) => {
    timer();
    displaySlider = setInterval(slider,3000);
    button.disabled = true;
    btnStop.disabled = false;
}

const timerDisplay = document.createElement('p');
document.body.append(timerDisplay);

const timer = () => {
        for (let seconds = 2; seconds >= 0; seconds--) {
            for (let milliseconds = 1000; milliseconds >= 0; milliseconds--) {
                setTimeout(() => {
                    timerDisplay.innerText = `${seconds} : ${milliseconds}`;
                }, (3000-1000*seconds-milliseconds));
            }
         }
}
timer();

